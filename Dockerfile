FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update
RUN apt-get install -y graphviz python3 python3-pip wget git
RUN pip3 install virtualenv
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.74.2/hugo_0.74.2_Linux-64bit.deb
RUN dpkg -i hugo_0.74.2_Linux-64bit.deb
WORKDIR /data

